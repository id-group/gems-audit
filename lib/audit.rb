require_relative 'audit/dependencies'
require_relative 'audit/project_gemfile'
require_relative 'audit/html_builder'

class Audit
  def self.build_html
    ProjectGemfile.new(ENV['PROJECT_URL_PREFIX']).fetch do |gemfile, lockfile|
      deps = Dependencies.new(gemfile, lockfile).to_a

      HtmlBuilder.new(deps).run
    end
  end
end
