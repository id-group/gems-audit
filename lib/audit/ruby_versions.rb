require 'hashie'

class RubyVersions < Hash
  include Hashie::Extensions::DeepLocate

  # Hardcode the known uses entries until the dynamic logic is implemented
  DEFAULT_RUBY_ACTIONS = {
  }

  def all
    deep_locate(->(k,v,o) {
      matches?(k, v) || uses_actions?(k, v)
    }).flat_map do |hash|
      hash.flat_map do |k, v|
        next v if matches?(k, v)
        next from_uses(v) if uses_actions?(k, v)
      end.compact.map(&:to_s)
    end.uniq
  end

  private

  def matches?(key, value)
    return false unless key.is_a?(String) && key.match?(/(ruby|rvm|image)/i)
    return false if value.is_a?(String) && value.include?('${{') # exclude templates like ${{ matrix.ruby }}

    !value.is_a?(Hash)
  end

  def uses_actions?(k, v)
    return unless k == 'uses'

    v.match?(/ruby.+yml@/)
  end

  # This method can potentially parse the uses entry
  # match = v.match(/^(?<namespace>[^\/].+)\/(?<project>[^\/].+)\/.github\/workflows\/(?<file>[^@].+)@(?<tag>.+)/)
  # And perform an HTTP request to fetch the versions
  def from_uses(v)
    DEFAULT_RUBY_ACTIONS[v]
  end
end
