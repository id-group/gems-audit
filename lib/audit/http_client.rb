require 'net/http'

class HttpClient
  REDIRECT_LIMIT = 3

  attr_reader :headers

  def initialize(headers = {})
    @headers = headers
  end

  def get(url, limit: REDIRECT_LIMIT)
    if limit <= 0
      puts "Fetching #{url}: Redirect limit exceeded"
    end

    uri = URI(url)
    req = Net::HTTP::Get.new(uri.request_uri, headers)
    response = Net::HTTP.start(uri.host, uri.port, use_ssl: uri.is_a?(URI::HTTPS)) { |http| http.request(req) }

    puts "Fetched #{url} with response: #{response.inspect}"

    case response
    when Net::HTTPSuccess     then response.body
    when Net::HTTPRedirection then get(response['location'], limit: limit - 1)
    else
    end
  end
end
