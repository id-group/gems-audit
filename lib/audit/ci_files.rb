require_relative 'gitlab_ci_files'
require_relative 'github_ci_files'

class CiFiles
  attr_reader :base_url, :version, :cache, :files

  class Build
    def initialize(current, upstream)
      @current = current
      @upstream = upstream
    end

    def current?
      @current
    end

    def upstream?
      @upstream
    end
  end

  def initialize(base_url, version, cache)
    @base_url = base_url
    @version = version
    @cache = cache
    @files = fetch_all
  end

  def build_for(ruby_version)
    Build.new(
      has_version?(:current_ruby_versions, ruby_version),
      has_version?(:ruby_versions, ruby_version)
    )
  end

  private

  def has_version?(type, target_ruby_version)
    files.flat_map(&type).uniq.any? do |ruby_version|
      # Covers cases like 3.0.x and ruby-3.0.x, but excludes x.3.0
      ruby_version.match?(/([^.]|^)#{target_ruby_version.to_s}/)
    end
  end

  def fetch_all
    return [] unless base_url

    if base_url.include?('gitlab')
      GitLabCiFiles.new(base_url, version, cache).all
    elsif base_url.include?('github')
      GitHubCiFiles.new(base_url, version, cache).all
    else
      []
    end
  end
end
