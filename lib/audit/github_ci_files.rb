require 'json'

require_relative 'http_client'
require_relative 'ci_file'

class GitHubCiFiles
  API_HOST = 'api.github.com'

  attr_reader :url, :base_url, :version, :cache, :http_client

  def initialize(base_url, version, cache)
    @base_url = base_url
    @url = api_url
    @version = version
    @cache = cache

    @http_client = HttpClient.new(
      { 'Accept' => 'application/vnd.github.raw', 'Authorization' => "Bearer #{ENV['GITHUB_TOKEN']}" }
    )
  end

  def all
    files = workflow_ci_files

    if files.empty?
      travis_ci_file = fetch_travis_ci_file
      files << travis_ci_file if travis_ci_file
    end

    files
  end

  private

  def api_url
    url = URI(base_url.delete_suffix('.git'))
    url.host = API_HOST

    path_parts = url.path.split('/')[0..2]
    url.path = File.join('/repos', *path_parts)
    url
  end

  def workflow_ci_files
    workflows.map do |workflow|
      content = cache.fetch("workflow-#{workflow['sha']}") do
        http_client.get(workflow['git_url']) || "---"
      end

      ci_file = CiFile.new(workflow['name'], workflow['html_url'], content)
      next if ci_file.ruby_versions.empty?

      new_content, tag = fetch_ci_of_current_version(workflow['name'], workflow['url'], version)

      ci_file.current_url = workflow['html_url'].gsub(/\/blob\/([^\/]+)\//, "/blob/#{tag}/")
      ci_file.current_ruby_versions = new_content
      ci_file
    end.compact
  end

  def workflows
    workflows_url = url.dup
    workflows_url.path = File.join(workflows_url.path, "contents/.github/workflows")

    # Fetch the newest version of workflows
    content = http_client.get(workflows_url)

    parsed_content = content ? JSON.parse(content) : []
    parsed_content.select { |workflow| workflow['name'].match?(/ya?ml$/) }
  end

  def fetch_ci_of_current_version(name, url, version)
    tag = "v#{version}"

    new_content = cache.fetch("workflow-#{name}-#{version}") do
      workflow_url = URI(url)
      workflow_url.query = "ref=#{tag}"

      content = http_client.get(workflow_url)
      next content if content

      actual_tag_name = fetch_actual_tag_name(version.to_s)
      next '---' if actual_tag_name == tag

      workflow_url.query = "ref=#{actual_tag_name}"
      http_client.get(workflow_url) || '---'
    end

    [new_content, cache.fetch("actual-tag-name-#{version.to_s}") { tag }]
  end

  def fetch_actual_tag_name(tag)
    cache.fetch("actual-tag-name-#{tag}") do
      tags_url = url.dup
      tags_url.path = File.join(tags_url.path, '/tags')

      tags = http_client.get(tags_url)
      next tag unless tags

      tag_object = JSON.parse(tags).map { |tag_object| tag_object['name'] }.find do |tag_name|
        tag_name.end_with?(tag)
      end || tag
    end
  end

  # This method is has multiple drawbacks:
  # - Cache is not invalidated correctly
  # - Default branch is stubbed to master
  # - Current Ruby versions are not calculated
  # Its value is to provide a link to .travis.yml files
  def fetch_travis_ci_file
    content = cache.fetch("workflow-travis.yml") do
      travis_url = url.dup
      travis_url.path = File.join(url.path, 'contents/.travis.yml')

      http_client.get(travis_url) || '---'
    end

    html_url = File.join(base_url, 'blob/master/.travis.yml')
    ci_file = CiFile.new('travis.yml', html_url, content)
    return if ci_file.ruby_versions.empty?

    ci_file.current_ruby_versions = '---'
    ci_file
  end
end
