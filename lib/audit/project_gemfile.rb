require_relative 'http_client'

require 'tempfile'

class ProjectGemfile
  attr_reader :project_url_prefix, :http_client

  def initialize(project_url_prefix)
    @project_url_prefix = project_url_prefix
    @http_client = HttpClient.new
  end

  def fetch
    files = ['Gemfile', 'Gemfile.lock'].map do |filename|
      url = File.join(project_url_prefix, filename)
      content = http_client.get(url)

      raise "Failed to fetch #{url}" unless content

      file = Tempfile.create(filename)
      file.write(content)
      file
    end

    yield files.map(&:path)

    files.each(&:close)
    files.each { |file| File.unlink(file) }
  end
end

