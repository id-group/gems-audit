require 'yaml'

require_relative 'ruby_versions'

class CiFile
  attr_reader :name, :url, :ruby_versions, :current_ruby_versions
  attr_accessor :current_url

  def initialize(name, url, content)
    @name = name
    @url = url
    @ruby_versions = from_yaml(content)
    @current_ruby_versions = []
  end

  def current_ruby_versions=(content)
    @current_ruby_versions = from_yaml(content)
  end

  private

  def from_yaml(content)
    yaml_content = YAML.safe_load(content, aliases: true)
    return [] unless yaml_content

    RubyVersions[yaml_content].all.sort
  end
end

