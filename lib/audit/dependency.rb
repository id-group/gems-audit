require_relative 'ci_files'
require_relative 'cache'
require_relative 'http_client'

require 'json'

class Dependency
  GEMS_API_URL = 'https://api.rubygems.org/api/v2/rubygems'
  DEFAULT_VENDOR_URL_PREFIX = 'https://gitlab.com/gitlab-org/gitlab/-/tree/master/'

  attr_reader :spec, :name, :path, :version, :source_url, :ci_files, :cache, :http_client

  def initialize(gem, spec)
    @http_client = HttpClient.new
    @cache = Cache.new(spec.full_name)
    @name = gem.name
    @path = gem.path
    @version = spec.version
    @spec = spec
  end

  def build
    puts "Gem: #{name}"

    if path
      @vendored = true
      @source_url = File.join(vendor_url_prefix, path)
      @ci_files = CiFiles.new(nil, version, cache)
    else
      @source_url = source_code_url(spec)
      @ci_files = CiFiles.new(@source_url, version, cache)
    end

    pp self
    puts '-' * 100

    self
  end

  def vendored?
    !@vendored.nil?
  end

  def source_url_host
    return if source_url.to_s.empty?
    return unless source_url =~ URI::regexp

    URI(source_url).host
  end

  private

  def vendor_url_prefix
    ENV['VENDOR_URL_PREFIX'] || DEFAULT_VENDOR_URL_PREFIX
  end

  def source_code_url(spec)
    content = cache.fetch('gem') do
      http_client.get("#{GEMS_API_URL}/#{spec.name}/versions/#{spec.version}.json")
    end

    JSON.parse(content).slice('source_code_uri', 'homepage_uri').values.compact.reject(&:empty?).first&.chomp('/')
  end
end
