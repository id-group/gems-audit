require 'json'
require 'cgi'

require_relative 'http_client'
require_relative 'ci_file'

class GitLabCiFiles
  attr_reader :url, :base_url, :version, :cache, :http_client

  def initialize(base_url, version, cache)
    @base_url = base_url
    @version = version
    @cache = cache

    @http_client = HttpClient.new(
      { 'Authorization' => "Bearer #{ENV['GITLAB_TOKEN']}" }
    )
  end

  def all
    [fetch_gitlab_ci_yml].compact
  end

  private

  def fetch_gitlab_ci_yml
    api_url = URI(base_url)
    api_url.path = File.join('/api/v4/projects', CGI.escape(api_url.path.delete_prefix('/')))

    content = cache.fetch('gitlab-default-branch') do
      http_client.get(api_url) || JSON.dump({})
    end

    default_branch = JSON.parse(content)['default_branch']
    return unless default_branch

    content = cache.fetch('gitlab-tree') do
      gitlab_tree_url = api_url.dup
      gitlab_tree_url.path = File.join(api_url.path, 'repository/tree')

      http_client.get(gitlab_tree_url) || JSON.dump([])
    end

    gitlab_ci_entry = JSON.parse(content).find { |entry| entry['name'] == '.gitlab-ci.yml' }
    return unless gitlab_ci_entry

    content = cache.fetch("gitlab-ci-yml-#{gitlab_ci_entry['id']}") do
      gitlab_ci_yml_url = api_url.dup
      gitlab_ci_yml_url.path = File.join(api_url.path, "repository/blobs/#{gitlab_ci_entry['id']}/raw")

      http_client.get(gitlab_ci_yml_url) || '---'
    end

    ci_file = CiFile.new('.gitlab-ci.yml', "#{base_url}/-/blob/#{default_branch}/.gitlab-ci.yml", content)
    return ci_file if ci_file.ruby_versions.empty?

    new_content = cache.fetch("gitlab-ci-yml-#{version}") do
      gitlab_ci_yml_url = api_url.dup
      gitlab_ci_yml_url.path = File.join(api_url.path, "repository/files/.gitlab-ci.yml/raw")
      gitlab_ci_yml_url.query = "ref=v#{version}"

      http_client.get(gitlab_ci_yml_url) || '---'
    end

    ci_file.current_url = File.join(base_url, "/-/blob/v#{version}/.gitlab-ci.yml")
    ci_file.current_ruby_versions = new_content
    ci_file
  end
end
