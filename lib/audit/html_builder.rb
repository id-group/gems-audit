require 'erb'

class HtmlBuilder
  OUTPUT = 'index.html'
  TEMPLATE = 'index.html.erb'

  attr_reader :dependencies

  def initialize(dependencies)
    @dependencies = dependencies
  end

  def run
    template = File.read(TEMPLATE)

    html = ERB.new(template).result_with_hash({ dependencies: dependencies })

    File.write(OUTPUT, html)
  end
end
