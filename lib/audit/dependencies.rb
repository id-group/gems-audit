require_relative 'dependency'

require 'bundler'

class Dependencies
  include Enumerable

  attr_reader :specs, :gems, :cache

  def initialize(gemfile, lockfile)
    @specs = Bundler::LockfileParser.new(Bundler.read_file(lockfile)).specs.to_h do |spec|
      [spec.name, spec]
    end

    @gems = Bundler::Definition.build(gemfile, nil, {}).dependencies.select do |dependency|
      (dependency.groups - [:test, :development, :monorepo, :coverage, :danger]).any?
    end
  end

  def each
    gems.sort_by(&:name).map do |gem|
      yield Dependency.new(gem, specs[gem.name]).build
    end
  end
end
