require 'fileutils'

class Cache
  DIR = 'tmp/cache'

  attr_reader :dirname

  def initialize(dirname)
    @dirname = File.join(DIR, dirname)

    FileUtils.mkdir_p(@dirname) unless File.directory?(@dirname)
  end

  def fetch(name)
    filename = File.join(dirname, name)

    if File.exist?(filename)
      puts "Read from cache: #{filename}"

      return File.read(filename)
    end

    yield&.tap { |result| File.write(filename, result) }
  end
end

