## Ruby gems audit

When a project is migrated to a new Ruby version, it always requires gems audit
to verify that the project, and all its dependencies are compatible with the new Ruby version.

This utility parses `Gemfile` and `Gemfile.lock` of a project, scan its dependencies and display which of them contain Ruby 3.0, 3.1, 3.2 CI builds configured:

- Facilitates the issues like https://gitlab.com/gitlab-org/gitlab/-/issues/404750+
- Automates some of the steps necessary for the files like https://docs.google.com/spreadsheets/d/167KJGE6wTA4euYtYeWqIl8B4BUgmonzajT4gJ14Hh-A.

The result is deployed to https://id-group.gitlab.io/gems-audit using [GitLab Pages](https://about.gitlab.com/stages-devops-lifecycle/pages/)

## How to use

1. Configure `GITLAB_TOKEN`, `GITHUB_TOKEN`, and `PROJECT_URL_PREFIX` environment variables and run a binary to build `index.html`

    ```shell
    $ bundle install

    $ bin/build_html
    ```

1. All the necessary data will be fetched and cached. Verbose logs will display the progress.
1. Open `index.html` to view the results

## How it works

1. `Gemfile` and `Gemfile.lock` are fetched based on `PROJECT_URL_PREFIX` variable.
1. The files are parsed to build a list of production dependencies and their versions.
1. Then each dependency is processed individually:
    1. If a gem specifies `path:` parameter in the `Gemfile`, it is considered vendored and the link to it is built as `ENV['VENDOR_URL_PREFIX'] + path`. No further actions are performed for vendored gems.
	1. A [RubyGems API request](https://guides.rubygems.org/rubygems-org-api-v2/) is performed to get a link to the source code.
	1. If the gem is hosted on GitLab:
        1. This library looks for `.gitlab-ci.yml` in the root of the project.
        1. If the file has been found, it is parsed to get Ruby versions used in CI for the latest version of the gem.
        1. Then another request is performed to get the state of `.gitlab-ci.yml` in `v#{current gem version}` tag in order to get Ruby versions used for CI in the current version of the gem.
        1. If a gem has been moved, it may not work correctly. This issue is fixed in https://gitlab.com/gitlab-org/gitlab/-/merge_requests/128642
    1. If the gem is hosted on GitHub:
        1. This library looks for `.github/workflows/*ya?ml` files
        1. Each file is parsed individually to build a list of Ruby versions used in CI for the latest version of the gem.
        1. Then another request is performed to get the state of each file in `v#{current gem version}` tag in order to get Ruby versions used for CI in the current version of the gem.
        1. If the `v#{current gem version}` tag is not found, a list of tags is fetched to find a tag that looks like `current gem version` to guess the tag of the `current gem version`.
        1. If the `.github/workflows/*ya?ml` list is empty, an additional request is performed to find `.travis.yml` file in the root of the project.
    1. The lists of the current Ruby versions and the versions that are used upstream are compared to get the status that is displayed in `index.html`:
        - 🟢 Both current and upstream versions contain a Ruby version. No upgrade is necessary.
        - 🟡 Current version of the gem does not contain a Ruby version, but the upstream version does. The upgrade is recommended.
        - 🔴 Neither the current nor upstream version contains a Ruby version. Upstream change or manual testing is recommended.

### How the CI files are parsed

CI files are generally YAML files that contain information about how to run tests and other checks and which environments and versions of it to use.

This library tries to guess the location of the Ruby versions that are used by CI. It iterates over the YAML structure by using [`deep_locate`](https://github.com/hashie/hashie/tree/master#deeplocate) and tries to find a key that matches `/(ruby|rvm)/i` and stores the values for the key if the values are not other nested objects.

The algorithm is not precise but it's sufficient to work on the CI files of the current GitLab project gems. It can be modified to work on new cases when they appear.
